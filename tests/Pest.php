<?php

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertGreaterThan;

/*
|--------------------------------------------------------------------------
| Test Case
|--------------------------------------------------------------------------
|
| The closure you provide to your test functions is always bound to a specific PHPUnit test
| case class. By default, that class is "PHPUnit\Framework\TestCase". Of course, you may
| need to change it using the "uses()" function to bind a different classes or traits.
|
*/

uses(Tests\TestCase::class)->in('Feature');

/*
|--------------------------------------------------------------------------
| Expectations
|--------------------------------------------------------------------------
|
| When you're writing tests, you often need to check that values meet certain conditions. The
| "expect()" function gives you access to a set of "expectations" methods that you can use
| to assert different things. Of course, you may extend the Expectation API at any time.
|
*/

expect()->extend('toBeOne', function () {
    return $this->toBe(1);
});

/*
|--------------------------------------------------------------------------
| Functions
|--------------------------------------------------------------------------
|
| While Pest is very powerful out-of-the-box, you may have some testing code specific to your
| project that you don't want to repeat in every file. Here you can also expose helpers as
| global functions to help you to reduce the number of lines of code in your test files.
|
*/

function arrayEquals(array $ar1, array $ar2): bool
{
    sort($ar1);
    sort($ar2);

    return $ar1 == $ar2;
}

function assertNewModelFieldEquals(Model $model, string $field): void
{
    $newModel = $model::query()->where($model->getKeyName(), $model->getKey())->first();

    assertEquals($model->{$field}, $newModel->{$field});
}

function assertNewModelFieldGreaterThan(Model $model, string $field): void
{
    $newModel = DB::table($model->getTable())->where($model->getKeyName(), $model->getKey())->first();
    $fieldOld = $model->{$field};
    $fieldNew = $newModel->{$field};
    if ($fieldOld instanceof CarbonInterface) {
        $fieldNew = Date::make($fieldNew);
    }

    assertGreaterThan($fieldOld, $fieldNew);
}

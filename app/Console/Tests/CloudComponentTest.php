<?php

namespace App\Console\Tests;

use App\Console\Commands\Cloud\CloudRecordProcessCommand;
use App\Domain\Cloud\Actions\DispatchToSync\DispatchCategoriesToSyncAction;
use App\Domain\Cloud\Actions\DispatchToSync\DispatchProductsToSyncAction;
use App\Domain\Cloud\Models\CloudIntegration;

use function Pest\Laravel\artisan;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'cloud');

test('Command CloudRecordProcessCommand enabled', function () {
    /** @var ComponentTestCase $this */
    CloudIntegration::factory()->enabled()->create();

    $this->mock(DispatchProductsToSyncAction::class)->shouldReceive('execute')->once();
    $this->mock(DispatchCategoriesToSyncAction::class)->shouldReceive('execute')->once();

    artisan(CloudRecordProcessCommand::class);
});

test('Command CloudRecordProcessCommand disabled', function () {
    /** @var ComponentTestCase $this */
    CloudIntegration::factory()->disabled()->create();

    $this->mock(DispatchProductsToSyncAction::class)->shouldReceive('execute')->never();
    $this->mock(DispatchCategoriesToSyncAction::class)->shouldReceive('execute')->never();

    artisan(CloudRecordProcessCommand::class);
});

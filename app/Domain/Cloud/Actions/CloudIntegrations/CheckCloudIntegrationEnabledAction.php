<?php

namespace App\Domain\Cloud\Actions\CloudIntegrations;

use App\Domain\Cloud\Models\CloudIntegration;
use Ensi\CloudApiSdk\Configuration;

class CheckCloudIntegrationEnabledAction
{
    public function __construct(protected Configuration $configuration)
    {
    }

    public function execute(): bool
    {
        if ($this->configuration->getPublicToken() && $this->configuration->getPrivateToken()) {
            return true;
        }

        /** @var CloudIntegration|null $integration */
        $integration = CloudIntegration::query()->whereStage()->first();
        if (!$integration || $integration->isDisabled()) {
            return false;
        }

        $this->configuration
            ->setPublicToken($integration->public_api_key)
            ->setPrivateToken($integration->private_api_key);

        return true;
    }
}

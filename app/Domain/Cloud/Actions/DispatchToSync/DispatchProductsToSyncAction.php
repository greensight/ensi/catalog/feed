<?php

namespace App\Domain\Cloud\Actions\DispatchToSync;

use App\Domain\Cloud\Jobs\SyncProductsJob;
use App\Domain\Offers\Models\Product;
use App\Domain\Support\SyncTimestampTypeEnum;
use Carbon\CarbonInterface;

class DispatchProductsToSyncAction implements DispatchEntityToSyncInterface
{
    public function __construct(protected DispatchEntityToSyncAction $dispatchEntityToIndexing)
    {
    }

    public function execute(): void
    {
        $this->dispatchEntityToIndexing->execute(
            type: SyncTimestampTypeEnum::CLOUD_PRODUCTS,
            queryBuilder: function (?CarbonInterface $from, CarbonInterface $to) {
                $query = Product::withTrashed()
                    ->where('cloud_fields_updated_at', '<', $to)
                    ->selectRaw('product_id as dispatch_id');

                if ($from) {
                    $query->where('cloud_fields_updated_at', '>=', $from);
                }

                return $query;
            },
            dispatch: function (array $ids) {
                SyncProductsJob::dispatch($ids);
            },
            logger: logger()->channel('cloud:dispatch-to-sync:products')
        );
    }
}

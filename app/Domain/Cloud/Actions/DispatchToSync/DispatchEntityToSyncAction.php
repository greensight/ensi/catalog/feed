<?php

namespace App\Domain\Cloud\Actions\DispatchToSync;

use App\Domain\Support\Models\SyncTimestamp;
use App\Domain\Support\SyncTimestampTypeEnum;
use App\Exceptions\ExceptionFormatter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Psr\Log\LoggerInterface;
use Throwable;

class DispatchEntityToSyncAction
{
    public function execute(
        SyncTimestampTypeEnum $type,
        callable $queryBuilder,
        callable $dispatch,
        LoggerInterface $logger
    ): void {
        $logger->info("=== Начато сканирование новых изменений для " . $type->value);

        try {
            /** @var SyncTimestamp|null $lastSync */
            $lastSync = SyncTimestamp::query()->whereType($type)->first();
            $now = now();

            if ($lastSync) {
                $logger->info("Ищу записи от " . $lastSync->last_schedule->toISOString());
            } else {
                $lastSync = SyncTimestamp::makeForType($type);
            }
            $logger->info("Ищу записи до " . $now->toISOString());

            /** @var Builder $query */
            $query = $queryBuilder($lastSync->last_schedule, $now);

            $query->chunk(500, function (Collection $results) use ($logger, $dispatch) {
                $ids = $results->pluck('dispatch_id');

                try {
                    $logger->info("Запускаю для синхронизации: {$ids->count()} шт.", [
                        'ids' => $ids->all(),
                    ]);

                    $dispatch($ids->all());

                    $logger->info("Запущено");
                } catch (Throwable $e) {
                    $logger->error(ExceptionFormatter::line("Ошибка при запуске", $e));
                }
            });

            $lastSync->last_schedule = $now;
            $lastSync->save();
        } catch (Throwable $e) {
            $logger->error(ExceptionFormatter::line("Ошибка при сканировании записей на синхронизацию", $e));
        }

        $logger->info("=== Закончено сканирование новых изменений");
    }
}

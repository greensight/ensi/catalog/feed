<?php

namespace App\Domain\Cloud\Jobs\Tests\TestCases;

use App\Domain\Cloud\Models\CloudIntegration;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\IndexesProductsRequest;
use Ensi\CloudApiSdk\Dto\Indexes\IndexesProducts\ProductAction\ProductAction;
use Mockery\Expectation;
use Tests\ComponentTestCase;

class SyncProductsJobTestCase extends ComponentTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        CloudIntegration::factory()->enabled()->create();
    }

    public function cloudIntegrationDisable(): void
    {
        CloudIntegration::query()->whereStage()->update(['integration' => false]);
    }

    public function &shouldSendProduct(): ?ProductAction
    {
        $action = null;
        /** @var Expectation $shouldReceive */
        $shouldReceive = $this->mockCloudApiIndexesApi()->shouldReceive('products');
        $shouldReceive
            ->once()
            ->withArgs(function (IndexesProductsRequest $apiRequest) use (&$action) {
                $action = $apiRequest->actions[0] ?? null;

                return count($apiRequest->actions) == 1;
            });

        return $action;
    }

    public function neverSendProduct(): void
    {
        $this->mockCloudApiIndexesApi()->shouldNotReceive('products');
    }
}

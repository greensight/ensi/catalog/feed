<?php

namespace App\Domain\Cloud\Jobs;

use App\Domain\Cloud\Actions\CloudIntegrations\CheckCloudIntegrationEnabledAction;
use App\Domain\Cloud\Actions\Sync\SyncCategoriesAction;
use App\Domain\Offers\Models\Category;

class SyncCategoriesJob extends SyncEntityJob
{
    public function __construct(array $ids)
    {
        parent::__construct($ids);

        $this->queue = "{cloud-sync-categories}";
    }

    public function handle(
        SyncCategoriesAction $action,
        CheckCloudIntegrationEnabledAction $checkAction,
    ): void {
        if (!$checkAction->execute()) {
            return;
        }

        $categories = Category::withTrashed()
            ->whereIn('category_id', $this->ids)
            ->get();

        $action->execute($categories);
    }
}

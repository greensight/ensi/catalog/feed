<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkProductAsUpdatedAction;
use App\Domain\Offers\Models\PropertyDirectoryValue;
use Illuminate\Database\Eloquent\Builder;

class PropertyDirectoryValueObserver
{
    public function __construct(
        protected MarkProductAsUpdatedAction $markAction
    ) {
    }

    public function saved(PropertyDirectoryValue $model): void
    {
        $this->markAction->execute(
            fn (Builder $query) => $query->whereHas('productPropertyValues', fn (Builder $queryValue) => $queryValue->where('directory_value_id', $model->directory_value_id)),
            $model->wasRecentlyCreated || $model->wasChanged(['value'])
        );
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт обновление по ProductPropertyValue,
    // и через него будет пометка на обновление
}

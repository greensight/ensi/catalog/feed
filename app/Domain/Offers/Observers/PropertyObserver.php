<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkProductAsUpdatedAction;
use App\Domain\Offers\Models\Property;
use Illuminate\Database\Eloquent\Builder;

class PropertyObserver
{
    public function __construct(
        protected MarkProductAsUpdatedAction $markAction
    ) {
    }

    public function saved(Property $model): void
    {
        $this->markAction->execute(
            fn (Builder $query) => $query->whereHas('productPropertyValues', fn (Builder $queryValue) => $queryValue->where('property_id', $model->property_id)),
            $model->wasRecentlyCreated || $model->wasChanged(['name', 'is_public', 'is_active'])
        );
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт обновление по ProductPropertyValue,
    // и через него будет пометка на обновление
}

<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ProductFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 *
 * @property int $product_id ID from PIM
 *
 * @property bool $allow_publish
 * @property string|null $main_image_file
 * @property int|null $brand_id Brand ID from PIM
 * @property string $name
 * @property string $code Code to use in URL
 * @property string|null $description
 * @property int $type Product type from ProductTypeEnum
 * @property string $vendor_code Article
 * @property string|null $barcode EAN
 * @property float|null $weight Net weight in kg
 * @property float|null $weight_gross Gross weight in kg
 * @property float|null $width Width in mm
 * @property float|null $height Height in mm
 * @property float|null $length Length in mm
 * @property bool $is_adult Is product 18+
 *
 * @property CarbonInterface|null $cloud_fields_updated_at
 * @property bool $cloud_created
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property Brand|null $brand
 * @property Collection<ProductPropertyValue> $productPropertyValues
 * @property-read ProductGroup|null $productGroup
 * @property-read ProductGroupProduct|null $productGroupProduct
 * @property-read Collection|Category[] $categories
 * @property-read Collection<Offer> $offers
 *
 * @property bool $is_migrated Was saved or created during record migration from master services
 */
class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    protected $casts = [
        'allow_publish' => 'bool',
        'brand_id' => 'int',
        'type' => 'int',
        'weight' => 'float',
        'weight_gross' => 'float',
        'width' => 'float',
        'height' => 'float',
        'length' => 'float',
        'is_adult' => 'bool',
        'is_migrated' => 'bool',
        'cloud_fields_updated_at' => 'datetime',
    ];

    public static function factory(): ProductFactory
    {
        return ProductFactory::new();
    }

    public function productPropertyValues(): HasMany
    {
        return $this->hasMany(ProductPropertyValue::class, 'product_id', 'product_id');
    }

    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class, 'brand_id', 'brand_id');
    }

    public function categories(): HasManyThrough
    {
        return $this->hasManyThrough(
            Category::class,
            CategoryProductLink::class,
            'product_id',
            'category_id',
            'product_id',
            'category_id',
        );
    }

    public function productGroup(): HasOneThrough
    {
        // Product can be placed only in one product group at the time
        return $this->hasOneThrough(
            ProductGroup::class,
            ProductGroupProduct::class,
            'product_id',
            'product_group_id',
            'product_id',
            'product_group_id'
        );
    }

    public function productGroupProduct(): HasOne
    {
        // Product can be placed only in one product group at the time
        return $this->hasOne(ProductGroupProduct::class, 'product_id', 'product_id');
    }

    public function offers(): HasMany
    {
        return $this->hasMany(Offer::class, 'product_id', 'product_id');
    }
}

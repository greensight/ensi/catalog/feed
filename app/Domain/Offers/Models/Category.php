<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\CategoryFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 *
 * @property int $category_id ID категории из PIM
 *
 * @property string $name Название
 * @property string $code ЧПУ код категории
 * @property int|null $parent_id Id родительской категории из PIM
 *
 * @property bool $is_active Признак активности, устанавливаемый пользователями
 * @property bool $is_real_active Признак активности с учетом иерархии
 *
 * @property CarbonInterface|null $cloud_fields_updated_at
 * @property bool $cloud_created
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';

    protected $casts = [
        'category_id' => 'int',
        'parent_id' => 'int',
        'is_active' => 'bool',
        'is_real_active' => 'bool',
        'is_migrated' => 'bool',
        'cloud_fields_updated_at' => 'datetime',
    ];

    public static function factory(): CategoryFactory
    {
        return CategoryFactory::new();
    }
}

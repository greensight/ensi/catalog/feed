<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Category;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<Category>
 */
class CategoryFactory extends BaseModelFactory
{
    protected $model = Category::class;

    public function definition(): array
    {
        return [
            'category_id' => $this->faker->modelId(),
            'code' => $this->faker->slug(),
            'name' => $this->faker->sentence(),
            'parent_id' => $this->faker->nullable()->modelId(),
            'is_active' => $this->faker->boolean(),
            'is_real_active' => $this->faker->boolean(),
            'cloud_created' => $this->faker->boolean(),
            'cloud_fields_updated_at' => $this->faker->dateTime(),
        ];
    }

    public function active(bool $active = true): self
    {
        return $this->state([
            'is_active' => $active,
            'is_real_active' => $active,
        ]);
    }
}

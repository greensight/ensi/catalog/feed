<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\OfferFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 *
 * @property int $offer_id - id оффера из Offers
 *
 * @property int $product_id - id товара из PIM
 *
 * @property int|null $base_price - базовая цена
 * @property int|null $price - итоговая цена со скидками
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property Product $product
 * @property Collection<Stock> $stocks
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class Offer extends Model
{
    protected $table = 'offers';

    protected $casts = [
        'offer_id' => 'int',
        'product_id' => 'int',
        'base_price' => 'int',
        'price' => 'int',
        'is_migrated' => 'bool',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }

    public function stocks(): HasMany
    {
        return $this->hasMany(Stock::class, 'offer_id', 'offer_id');
    }

    public static function factory(): OfferFactory
    {
        return OfferFactory::new();
    }
}

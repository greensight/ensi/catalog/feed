<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\PropertyDirectoryValueFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $directory_value_id ID значения справочника из PIM
 *
 * @property int $property_id ID свойства из PIM
 * @property string|null $name Название значения
 * @property string|null $code Символьный код
 * @property string $value Значение
 * @property string $type Тип хранимого значения
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class PropertyDirectoryValue extends Model
{
    protected $table = 'property_directory_values';

    protected $casts = [
        'directory_value_id' => 'int',
        'property_id' => 'int',
        'is_migrated' => 'bool',
    ];

    public static function factory(): PropertyDirectoryValueFactory
    {
        return PropertyDirectoryValueFactory::new();
    }
}

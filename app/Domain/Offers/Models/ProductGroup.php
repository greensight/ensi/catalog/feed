<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ProductGroupFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 *
 * @property int $product_group_id - id товарной склейки из PIM
 *
 * @property int $category_id Id категории из PIM
 * @property string $name Название
 * @property int|null $main_product_id Id главного товара из PIM
 * @property bool $is_active Признак активности из PIM
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read Category $category Категория
 * @property-read Product $mainProduct Главный товар
 * @property-read Collection<Product> $products Товары склейки
 * @property-read Collection<ProductGroupProduct> $productLinks Привязки товаров к склейке
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class ProductGroup extends Model
{
    protected $table = 'product_groups';

    protected $casts = [
        'product_group_id' => 'int',
        'category_id' => 'int',
        'main_product_id' => 'int',
        'is_active' => 'float',
        'is_migrated' => 'bool',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id', 'category_id');
    }

    public function mainProduct(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'main_product_id', 'product_id');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(
            Product::class,
            'product_group_product',
            'product_group_id',
            'product_id',
            'product_group_id',
            'product_id'
        );
    }

    public function productLinks(): HasMany
    {
        return $this->hasMany(ProductGroupProduct::class, 'product_group_id', 'product_group_id');
    }

    public static function factory(): ProductGroupFactory
    {
        return ProductGroupFactory::new();
    }
}

<?php

namespace App\Domain\Sitemap\Tests;

use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Product;
use App\Domain\Sitemap\Actions\GenerateSitemapAction;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Assert;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('component', 'sitemap');

test('Action GenerateSitemapAction successfully', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    $disk = Storage::fake($fs->publicDiskName());

    $category = Category::factory()->create();
    Product::factory()->inCategories([$category])->create();

    $action = resolve(GenerateSitemapAction::class);
    $action->setLogger(Log::getLogger())->execute();

    Assert::assertTrue($disk->exists('sitemap/sitemapindex.xml'));
    Assert::assertTrue($disk->exists('sitemap/category.xml'));
    Assert::assertTrue($disk->exists('sitemap/products.xml'));
    Assert::assertTrue($disk->exists('sitemap/image.xml'));
    Assert::assertTrue($disk->exists('sitemap/inner.xml'));
});

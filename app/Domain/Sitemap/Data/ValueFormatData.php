<?php

namespace App\Domain\Sitemap\Data;

class ValueFormatData
{
    /** @var string Базовый формат даты в сайтмапах */
    public const DATE = 'Y-m-d';
}

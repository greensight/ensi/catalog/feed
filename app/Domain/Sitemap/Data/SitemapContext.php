<?php

namespace App\Domain\Sitemap\Data;

use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Product;
use Illuminate\Support\LazyCollection;

class SitemapContext
{
    /**
     * @param string $baseDomain - Домен публички сайта
     * @param string $directoryPath - Путь к директории сохранения XML-файлов сайтмапов
     * @param LazyCollection<Product> $products - Данные по продукту
     * @param LazyCollection<Category> $categories - Данные по категории
     */
    public function __construct(
        public string $baseDomain,
        public string $directoryPath,
        public LazyCollection $products,
        public LazyCollection $categories,
    ) {
    }
}

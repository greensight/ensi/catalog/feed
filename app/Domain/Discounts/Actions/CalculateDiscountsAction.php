<?php

namespace App\Domain\Discounts\Actions;

use App\Domain\Offers\Models\Offer;
use Ensi\MarketingClient\Api\CalculatorsApi;
use Ensi\MarketingClient\ApiException;
use Ensi\MarketingClient\Dto\CalculateCatalogOffer;
use Ensi\MarketingClient\Dto\CalculateCatalogRequest;
use Ensi\MarketingClient\Dto\CalculateCatalogResponseData;
use Illuminate\Support\Collection;

class CalculateDiscountsAction
{
    public function __construct(protected readonly CalculatorsApi $calculatorsApi)
    {
    }

    public function execute(array $offerIds): void
    {
        /** @var Collection<Offer> $offers */
        $offers = Offer::query()
            ->whereIn('offer_id', $offerIds)
            ->get()
            ->keyBy('offer_id');

        $data = $this->calculateDiscount($offers);

        if ($data) {
            $this->saveDiscount($data, $offers);
        }
    }

    /**
     * @param Collection<Offer> $offers
     * @throws ApiException
     */
    protected function calculateDiscount(Collection $offers): ?CalculateCatalogResponseData
    {
        $requestOffers = [];

        foreach ($offers as $offer) {
            if ($offer->base_price) {
                $requestOffer = new CalculateCatalogOffer();
                $requestOffer->setOfferId($offer->offer_id);
                $requestOffer->setProductId($offer->product_id);
                $requestOffer->setCost($offer->base_price);
                $requestOffers[] = $requestOffer;
            }
        }

        if (empty($requestOffers)) {
            return null;
        }

        $request = new CalculateCatalogRequest();
        $request->setOffers($requestOffers);

        return $this->calculatorsApi->calculateCatalog($request)->getData();
    }

    /**
     * @param Collection<Offer> $offers
     */
    protected function saveDiscount(CalculateCatalogResponseData $data, Collection $offers): void
    {
        foreach ($data->getOffers() as $calculatedOffer) {
            /** @var Offer $offer */
            $offer = $offers->get($calculatedOffer->getOfferId());

            $offer->price = $calculatedOffer->getPrice();
            $offer->save();
        }
    }
}

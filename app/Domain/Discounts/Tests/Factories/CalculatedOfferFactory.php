<?php

namespace App\Domain\Discounts\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\CalculatedOffer;

class CalculatedOfferFactory extends BaseApiFactory
{
    protected array $discounts = [];

    protected function definition(): array
    {
        $cost = $this->faker->numberBetween(1, 10_000_00);

        return [
            'offer_id' => $this->faker->modelId(),
            'price' => $this->faker->numberBetween(1, $cost),
            'cost' => $cost,
            'discounts' => $this->makeDiscounts(),
        ];
    }

    public function make(array $extra = []): CalculatedOffer
    {
        return new CalculatedOffer($this->makeArray($extra));
    }

    protected function makeDiscounts(): array
    {
        $count = $this->discounts ? count($this->discounts) : $this->faker->numberBetween(1, 10);
        $discounts = [];
        for ($i = 0; $i < $count; $i++) {
            $discounts[] = CalculatedDiscountFactory::new()->make($this->discounts[$i] ?? []);
        }

        return $discounts;
    }

    public function withDiscounts(array $discount): static
    {
        $this->discounts[] = $discount;

        return $this;
    }
}

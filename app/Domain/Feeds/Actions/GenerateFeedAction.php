<?php

namespace App\Domain\Feeds\Actions;

use App\Domain\FeedBuilder\Data\FeedInfo;
use App\Domain\FeedBuilder\FeedBuilder;
use App\Domain\FeedBuilder\Files\FileFactory;
use App\Domain\Feeds\Models\FeedSettings;

class GenerateFeedAction
{
    private const TEMP_PREFIX = 'temp_';

    public function __construct(
        protected LoadFeedDataAction $loadFeedData,
        protected readonly FileFactory $fileFactory,
        protected PublishFeedAction $publishFeedAction,
    ) {
    }

    public function execute(FeedSettings $feedSettings): FeedInfo
    {
        $data = $this->loadFeedData->execute($feedSettings);
        $feedFile = $this->fileFactory->execute($feedSettings->platform, $feedSettings->type);

        $feedBuilder = new FeedBuilder($feedFile, $data);

        $feedInfo = $feedBuilder->build($this->getTempName($feedSettings->code));

        if ($feedInfo->isSuccessful()) {
            $this->publishFeedAction->execute(
                feedSettings: $feedSettings,
                tempPath: $feedInfo->getFilePath(),
                publicPath: $this->getOriginalName($feedInfo->getFilePath()),
            );
        }

        return $feedInfo;
    }

    protected function getTempName(string $name): string
    {
        return self::TEMP_PREFIX . $name;
    }

    protected function getOriginalName(string $name): string
    {
        return str_replace(self::TEMP_PREFIX, "", $name);
    }
}

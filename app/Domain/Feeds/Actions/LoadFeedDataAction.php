<?php

namespace App\Domain\Feeds\Actions;

use App\Domain\FeedBuilder\Data\EntityData;
use App\Domain\FeedBuilder\Data\EntityData\CategoryData;
use App\Domain\FeedBuilder\Data\EntityData\CurrencyData;
use App\Domain\FeedBuilder\Data\EntityData\OfferData;
use App\Domain\FeedBuilder\Data\EntityData\OfferParameter;
use App\Domain\FeedBuilder\Data\EntityData\ShopInfoData;
use App\Domain\Feeds\Models\FeedSettings;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Offer;
use App\Exceptions\ExceptionFormatter;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\LazyCollection;
use Psr\Log\LoggerInterface;

class LoadFeedDataAction
{
    public const CHUNK_SIZE = 500;
    private LoggerInterface $logger;

    private FeedSettings $feedSettings;
    private CurrencyData $currency;

    public function __construct()
    {
        $this->logger = Log::channel('feeds');
    }

    public function execute(FeedSettings $feedSettings, ?CurrencyData $currencyData = null): EntityData
    {
        $this->feedSettings = $feedSettings;
        $this->currency = $currencyData ?? CurrencyData::default();

        return new EntityData(
            shopInfo: $this->makeShopInfo(),
            currency: $this->currency,
            categories: $this->loadCategories(),
            offers: $this->loadOffers(),
        );
    }

    public function makeShopInfo(): ShopInfoData
    {
        return new ShopInfoData(
            name: $this->feedSettings->shop_name,
            company: $this->feedSettings->shop_company,
            url: $this->feedSettings->shop_url,
        );
    }

    public function loadCategories(): LazyCollection
    {
        $categoryQuery = Category::query();

        if ($this->feedSettings->active_category) {
            $categoryQuery->where('is_active', true);
        }

        if ($this->feedSettings->active_product) {
            $categoryQuery->where('is_real_active', true);
        }

        return $categoryQuery
            ->lazyById(self::CHUNK_SIZE)
            ->map(function (Category $category) {
                return new CategoryData(
                    id: $category->category_id,
                    parentId: $category->parent_id,
                    name: $category->name,
                );
            });
    }

    public function loadOffers(): LazyCollection
    {
        $offerQuery = Offer::query();

        if ($this->feedSettings->active_product) {
            $offerQuery->whereHas('product', fn (Builder $query) => $query->where('allow_publish', true));
        }

        return $offerQuery
            ->whereNotNull('base_price')
            ->whereHas('stocks')
            ->with([
                'stocks',
                'product.brand', 'product.categories', 'product.productGroup',
                'product.productPropertyValues.property', 'product.productPropertyValues.directoryValue',
            ])
            ->lazyById(self::CHUNK_SIZE)
            ->map(function (Offer $offer) {
                try {
                    $offerData = new OfferData(
                        id: $offer->offer_id,
                        url: "{$this->feedSettings->shop_url}/{$offer->offer_id}",
                        price: $offer->price ?? $offer->base_price,
                        cost: $offer->base_price,
                        currencyId: $this->currency->id,
                        stockQty: $offer->stocks->first()->qty,
                        vendorName: $offer->product->brand?->name,
                        vendorCode: $offer->product->vendor_code,
                        categoryId: $offer->product->categories->first()->category_id,
                        name: $offer->product->name,
                        groupId: $offer->product->productGroup?->product_group_id,
                        mailImageFile: $offer->product->main_image_file,
                        description: $offer->product->description,
                        isAdult: $offer->product->is_adult,
                        barcode: $offer->product->barcode,
                        weight: $offer->product->weight_gross,
                        width: $offer->product->width,
                        height: $offer->product->height,
                        length: $offer->product->length,
                    );

                    foreach ($offer->product->productPropertyValues ?? [] as $propertyValue) {
                        $property = $propertyValue->property;
                        $value = $propertyValue->getPublicValue();

                        if (!$property || !$value || !$property->is_public || !$property->is_active) {
                            continue;
                        }

                        $offerData->addParameter(
                            new OfferParameter(name: $property->name, value: $value)
                        );
                    }

                    return $offerData;
                } catch (Exception $e) {
                    $this->logger->error(ExceptionFormatter::line("Ошибка при отображении оффера: {$offer->offer_id}", $e));

                    return null;
                }
            })
            ->filter();
    }
}

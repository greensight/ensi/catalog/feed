<?php

namespace App\Domain\Feeds\Actions;

use App\Domain\Feeds\Models\Feed;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Storage;

class ArchiveFeedAction
{
    public function __construct(protected EnsiFilesystemManager $fs)
    {
    }

    public function execute(Feed $feed): void
    {
        $disk = Storage::disk($this->fs->publicDiskName());
        $now = Date::now()->toIso8601String();

        $feed->code = $this->addDateMark($feed->code, $now);

        $newFilePath = $this->addDateMark($feed->file, $now);
        $disk->move($feed->file, $newFilePath);
        $feed->file = $newFilePath;

        $feed->save();
    }

    protected function addDateMark(string $code, string $date): string
    {
        $newPostfix = '-' . $date;
        $extension = pathinfo($code, PATHINFO_EXTENSION);

        if (empty($extension)) {
            return $code . $newPostfix;
        }

        $extension = '.' . $extension;

        return str_replace($extension, $newPostfix . $extension, $code);
    }
}

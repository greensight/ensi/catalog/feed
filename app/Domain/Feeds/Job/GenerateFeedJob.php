<?php

namespace App\Domain\Feeds\Job;

use App\Domain\Feeds\Actions\GenerateFeedAction;
use App\Domain\Feeds\Models\FeedSettings;
use App\Exceptions\ExceptionFormatter;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class GenerateFeedJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(protected FeedSettings $feedSettings)
    {
    }

    public function handle(GenerateFeedAction $action): void
    {
        $logger = Log::channel('feeds');

        try {
            $info = $action->execute($this->feedSettings);
            $logInfo = $info->getStringInfo($this->feedSettings->platform);

            $logger->info("Фид c кодом {$this->feedSettings->code} \n$logInfo");
        } catch (Exception $e) {
            $logger->error(ExceptionFormatter::line("Ошибка генерации фида {$this->feedSettings->code}", $e));
        }
    }
}

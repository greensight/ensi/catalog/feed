<?php

namespace App\Domain\Feeds\Models\Tests\Factories;

use App\Domain\Feeds\Models\Feed;
use App\Domain\Feeds\Models\FeedSettings;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedPlatformEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedTypeEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<FeedSettings>
 */
class FeedSettingsFactory extends BaseModelFactory
{
    protected $model = FeedSettings::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->word(),
            'active' => $this->faker->boolean(),
            'type' => $this->faker->randomEnum(FeedTypeEnum::cases()),
            'platform' => $this->faker->randomEnum(FeedPlatformEnum::cases()),
            'active_product' => $this->faker->boolean(),
            'active_category' => $this->faker->boolean(),
            'shop_name' => $this->faker->sentence(),
            'shop_url' => $this->faker->sentence(),
            'shop_company' => $this->faker->sentence(),
            'update_time' => $this->faker->numberBetween(1, 10),
            'delete_time' => $this->faker->numberBetween(1, 10),
        ];
    }

    public function active(): self
    {
        return $this->state([
            'active' => true,
        ]);
    }

    public function timeGenerate(bool $withFeed = false): self
    {
        $factory = $this->active();

        if (!$withFeed) {
            return $factory;
        }

        return $factory
            ->afterCreating(function (FeedSettings $feedSettings) {
                Feed::factory()->for($feedSettings)->create([
                    'created_at' => now()->subHours($feedSettings->update_time + 1),
                ]);
            });
    }
}

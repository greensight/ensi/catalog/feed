<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\PropertyDirectoryValue;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $property_id ID свойства
 * @property string $type Тип хранимого значения
 * @property string|null $name Название значения
 * @property string $value Значение
 * @property string|null $code Символьный код
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class PropertyDirectoryValuePayload extends Payload
{
}

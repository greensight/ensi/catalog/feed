<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Offers\Models\ProductPropertyValue;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class PublishedPropertyValueEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),

            'property_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),

            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'name' => $this->faker->nullable()->sentence(),
            'code' => $this->faker->nullable()->modelId(),
            'directory_value_id' => $this->faker->nullable()->modelId(),
            'value' => $this->faker->nullable()->word(),

            'deleted_at' => $this->faker->nullable()->date(self::DATE_TIME_FORMAT),

            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),
        ];
    }

    public function forModel(ProductPropertyValue $model): self
    {
        return $this->attributes([
            'id' => $model->product_property_value_id,

            'property_id' => $model->property_id,
            'product_id' => $model->product_id,

            'type' => $model->type,
            'name' => $model->name,
            'directory_value_id' => $model->directory_value_id,
            'value' => $model->value,

            'created_at' => $model->created_at->format(self::DATE_TIME_FORMAT),
            'updated_at' => $model->updated_at->format(self::DATE_TIME_FORMAT),
        ]);
    }
}

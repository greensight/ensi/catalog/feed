<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Offers\Models\ProductGroupProduct;

class ProductGroupProductEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'product_group_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),
        ];
    }

    public function forModel(ProductGroupProduct $model): static
    {
        return $this->attributes([
            'id' => $model->product_group_product_id,
            'product_group_id' => $model->product_group_id,
            'product_id' => $model->product_id,
            'created_at' => $model->created_at->format(self::DATE_TIME_FORMAT),
            'updated_at' => $model->updated_at->format(self::DATE_TIME_FORMAT),
        ]);
    }
}

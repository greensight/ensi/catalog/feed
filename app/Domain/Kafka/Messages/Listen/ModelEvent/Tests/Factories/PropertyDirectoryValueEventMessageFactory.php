<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Domain\Offers\Models\PropertyDirectoryValue;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class PropertyDirectoryValueEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),

            'property_id' => $this->faker->modelId(),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'name' => $this->faker->nullable()->sentence(),
            'value' => $this->faker->word(),
            'code' => $this->faker->nullable()->modelId(),

            'created_at' => $this->faker->date(self::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(self::DATE_TIME_FORMAT),
        ];
    }

    public function forModel(PropertyDirectoryValue $model): static
    {
        return $this->attributes([
            'id' => $model->directory_value_id,
            'property_id' => $model->property_id,
            'type' => $model->type,
            'name' => $model->name,
            'value' => $model->value,
            'code' => $model->code,
            'created_at' => $model->created_at->format(self::DATE_TIME_FORMAT),
            'updated_at' => $model->updated_at->format(self::DATE_TIME_FORMAT),
        ]);
    }
}

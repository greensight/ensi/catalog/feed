<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\PropertyDirectoryValue\PropertyDirectoryValueEventMessage;
use App\Domain\Offers\Models\PropertyDirectoryValue;
use RdKafka\Message;

class ListenPropertyDirectoryValueAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = PropertyDirectoryValueEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'property_id',
                'name',
                'code',
                'value',
                'type',
            ],
            findModel: fn () => PropertyDirectoryValue::query()->where('directory_value_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new PropertyDirectoryValue();
                $model->directory_value_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (PropertyDirectoryValue $model) use ($modelPayload) {
                $model->property_id = $modelPayload->property_id;
                $model->name = $modelPayload->name;
                $model->code = $modelPayload->code;
                $model->value = $modelPayload->value;
                $model->type = $modelPayload->type;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}

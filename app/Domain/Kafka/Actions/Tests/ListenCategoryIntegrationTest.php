<?php

use App\Domain\Kafka\Actions\Listen\ListenCategoryAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Category\CategoryEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Offers\Models\Category;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Facades\Date;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\isSoftDeletableModel;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-category');

test("Action ListenCategoryAction create success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;
    Date::setTestNow(now());

    /** @var Category $categoryParent */
    $categoryParent = Category::factory()->create(['cloud_fields_updated_at' => now()->subDay(), 'parent_id' => null]);

    $categoryId = 1;
    $message = CategoryEventMessage::factory()
        ->attributes(['id' => $categoryId, 'parent_id' => $categoryParent->category_id])
        ->event(ModelEventMessage::CREATE)
        ->make();

    /** @var Category $categoryChild1 */
    $categoryChild1 = Category::factory()->create(['cloud_fields_updated_at' => now()->subDay(), 'parent_id' => $categoryId]);
    /** @var Category $categoryChild2 */
    $categoryChild2 = Category::factory()->create(['cloud_fields_updated_at' => now()->subDay(), 'parent_id' => $categoryChild1->category_id]);

    // Создаём товар, который связан с этой моделью и должен обновиться cloud_fields_updated_at
    $productMark = ProductLinkedFactory::createFromCategory($categoryId);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromCategory($categoryId + 1);

    resolve(ListenCategoryAction::class)->execute($message);

    assertDatabaseHas(Category::class, [
        'category_id' => $categoryId,
        'cloud_fields_updated_at' => now()->format((new Category())->getDateFormat()),
    ]);
    assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
    assertNewModelFieldGreaterThan($categoryChild1, 'cloud_fields_updated_at');
    assertNewModelFieldGreaterThan($categoryChild2, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($categoryParent, 'cloud_fields_updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenCategoryAction update success", function (
    string $updatedField,
    mixed $newValue,
    bool $isUpdated,
    bool $isCloudCategoryUpdated,
    bool $isCloudParentUpdated,
    bool $isCloudProductUpdated,
) {
    /** @var IntegrationTestCase $this */

    /** @var Category $categoryParent */
    $categoryParent = Category::factory()->create(['cloud_fields_updated_at' => now()->subDay(), 'parent_id' => null]);
    /** @var Category $category */
    $category = Category::factory()->create([
        'cloud_fields_updated_at' => now()->subDay(),
        'parent_id' => $categoryParent->category_id,
        'is_real_active' => false,
    ]);
    /** @var Category $categoryChild1 */
    $categoryChild1 = Category::factory()->create(['cloud_fields_updated_at' => now()->subDay(), 'parent_id' => $category->category_id]);
    /** @var Category $categoryChild2 */
    $categoryChild2 = Category::factory()->create(['cloud_fields_updated_at' => now()->subDay(), 'parent_id' => $categoryChild1->category_id]);

    $updatedAt = $category->updated_at->addDay();
    $message = CategoryEventMessage::factory()
        ->forModel($category)
        ->attributes([$updatedField => $newValue, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => [$updatedField]]);

    // Создаём товар, который связан с этой моделью и должен обновиться updated_at
    $productMark = ProductLinkedFactory::createFromCategory($category->category_id);
    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromCategory($category->category_id + 1);

    $categoryOther = Category::factory()->create([
        'cloud_fields_updated_at' => now()->subDay(),
    ]);

    resolve(ListenCategoryAction::class)->execute($message);

    if ($isCloudCategoryUpdated) {
        assertNewModelFieldGreaterThan($category, 'cloud_fields_updated_at');
    } else {
        assertNewModelFieldEquals($category, 'cloud_fields_updated_at');
    }
    if ($isCloudParentUpdated) {
        assertNewModelFieldGreaterThan($categoryChild1, 'cloud_fields_updated_at');
        assertNewModelFieldGreaterThan($categoryChild2, 'cloud_fields_updated_at');
        assertNewModelFieldEquals($categoryParent, 'cloud_fields_updated_at');
    } else {
        assertNewModelFieldEquals($categoryChild1, 'cloud_fields_updated_at');
        assertNewModelFieldEquals($categoryChild2, 'cloud_fields_updated_at');
        assertNewModelFieldEquals($categoryParent, 'cloud_fields_updated_at');
    }
    $needUpdatedAt = $isUpdated ? $updatedAt : $category->updated_at;
    $needField = $isUpdated ? $newValue : $category->getAttribute($updatedField);
    $category->refresh();
    assertEquals($needUpdatedAt, $category->updated_at);
    assertEquals($needField, $category->getAttribute($updatedField));

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isCloudProductUpdated) {
        assertNewModelFieldGreaterThan($productMark, 'cloud_fields_updated_at');
    } else {
        assertNewModelFieldEquals($productMark, 'cloud_fields_updated_at');
    }
    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
    assertNewModelFieldEquals($categoryOther, 'cloud_fields_updated_at');
})->with([
    // $updatedField, $newValue, $isUpdated, $isCloudCategoryUpdated, $isCloudParentUpdated, $isCloudProductUpdated,
    ['name',            'Новое имя',    true,  true,  false,  true],
    ['code',            'new_code',     true,  false, false,  false],
    ['parent_id',       25,             true,  true,  true,   true],
    ['is_active',       true,           true,  false, false,  false],
    ['is_real_active',  true,           true,  true, false,  false],
    ['other_field',     'value',        false, false, false,  false],
]);

test("Action ListenCategoryAction delete success", function () {
    /** @var IntegrationTestCase $this */

    /** @var Category $category */
    $category = Category::factory()->create(['cloud_fields_updated_at' => now()->subDay()]);
    $message = CategoryEventMessage::factory()
        ->forModel($category)
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар, который связан с этой моделью и не должна обновиться cloud_fields_updated_at, т.к. при удалении не помечаем
    $product = ProductLinkedFactory::createFromCategory($category->category_id);

    resolve(ListenCategoryAction::class)->execute($message);

    isSoftDeletableModel($category);
    assertNewModelFieldGreaterThan($category, 'cloud_fields_updated_at');
    // Удаление произошло, но пометки нет
    assertNewModelFieldEquals($product, 'cloud_fields_updated_at');
});

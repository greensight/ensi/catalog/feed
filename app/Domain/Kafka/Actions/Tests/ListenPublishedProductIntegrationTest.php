<?php

use App\Domain\Kafka\Actions\Listen\ListenPublishedProductAction;
use App\Domain\Kafka\Actions\Tests\Factories\ProductLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct\PublishedProductEventMessage;
use App\Domain\Offers\Models\Product;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Facades\Date;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\isSoftDeletableModel;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-published-product');

test("Action ListenPublishedProductAction create success", function (?bool $always) {
    /** @var IntegrationTestCase $this */
    FakerProvider::$optionalAlways = $always;
    Date::setTestNow(now());

    $productId = 1;
    $message = PublishedProductEventMessage::factory()
        ->attributes(['id' => $productId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    resolve(ListenPublishedProductAction::class)->execute($message);

    assertDatabaseHas(Product::class, [
        'product_id' => $productId,
        'cloud_fields_updated_at' => now()->format((new Product())->getDateFormat()),
    ]);
})->with(FakerProvider::$optionalDataset);

test("Action ListenPublishedProductAction update success", function (string $updatedField, mixed $newValue, bool $isUpdated, bool $isCloudUpdated) {
    /** @var IntegrationTestCase $this */

    /** @var Product $product */
    $product = Product::factory()->create(['allow_publish' => false, 'is_adult' => false]);
    $updatedAt = $product->updated_at->addDay();
    $message = PublishedProductEventMessage::factory()
        ->forModel($product)
        ->attributes([$updatedField => $newValue, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => [$updatedField]]);

    // Создаём товар, который не связан с этой моделью, чтобы проверить что лишнее не помечается
    $productOther = ProductLinkedFactory::createFromProduct($product->product_id + 1);

    resolve(ListenPublishedProductAction::class)->execute($message);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isCloudUpdated) {
        assertNewModelFieldGreaterThan($product, 'cloud_fields_updated_at');
    } else {
        assertNewModelFieldEquals($product, 'cloud_fields_updated_at');
    }
    $needUpdatedAt = $isUpdated ? $updatedAt : $product->updated_at;
    $needField = $isUpdated ? $newValue : $product->getAttribute($updatedField);
    $product->refresh();
    assertEquals($needUpdatedAt, $product->updated_at);
    assertEquals($needField, $product->getAttribute($updatedField));

    assertNewModelFieldEquals($productOther, 'cloud_fields_updated_at');
})->with([
    ['allow_publish', true, true, true],
    ['main_image_file', 'https://new-site.com', true, true],
    ['brand_id', 33, true, true],
    ['name', 'Новое имя', true, true],
    ['code', 'new_code', true, false],
    ['description', 'Новое описание', true, true],
    ['vendor_code', '323242424242', true, true],
    ['barcode', '323242424242', true, true],
    ['weight', '11.11', true, false],
    ['weight_gross', '11.11', true, false],
    ['width', '11.11', true, false],
    ['height', '11.11', true, false],
    ['length', '11.11', true, false],
    ['is_adult', true, true, false],
    ['other_field', 'value', false, false],
]);

test("Action ListenPublishedProductAction delete success", function () {
    /** @var IntegrationTestCase $this */

    $product = ProductLinkedFactory::createFromProduct();
    $message = PublishedProductEventMessage::factory()
        ->attributes(['id' => $product->product_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    resolve(ListenPublishedProductAction::class)->execute($message);

    isSoftDeletableModel($product);
    assertNewModelFieldGreaterThan($product, 'cloud_fields_updated_at');
});

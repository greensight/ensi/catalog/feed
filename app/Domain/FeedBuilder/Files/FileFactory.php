<?php

namespace App\Domain\FeedBuilder\Files;

use App\Domain\FeedBuilder\Actions\Categories\SetYaCategoriesAction;
use App\Domain\FeedBuilder\Actions\Offers\SetYaDirectOffersAction;
use App\Domain\FeedBuilder\Actions\Offers\SetYaMarketOffersAction;
use App\Domain\FeedBuilder\Contracts\FeedFile;
use App\Domain\FeedBuilder\Files\FileTypes\YandexXmlFile;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedPlatformEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\FeedTypeEnum;
use Illuminate\Contracts\Container\Container;

class FileFactory
{
    public function __construct(protected readonly Container $container)
    {
    }

    public function execute(FeedPlatformEnum $platform, FeedTypeEnum $type): FeedFile
    {
        return match ($platform) {
            FeedPlatformEnum::YANDEX_MARKET => $this->getYandexMarketFile($type),
            FeedPlatformEnum::YANDEX_DIRECT => $this->getYandexDirectBuilder($type),
        };
    }

    private function getYandexMarketFile(FeedTypeEnum $type): FeedFile
    {
        return match ($type) {
            FeedTypeEnum::YML => new YandexXmlFile(
                setCategories: $this->container->make(SetYaCategoriesAction::class),
                setOffers: $this->container->make(SetYaMarketOffersAction::class),
            ),
        };
    }

    private function getYandexDirectBuilder(FeedTypeEnum $type): FeedFile
    {
        return match ($type) {
            FeedTypeEnum::YML => new YandexXmlFile(
                setCategories: $this->container->make(SetYaCategoriesAction::class),
                setOffers: $this->container->make(SetYaDirectOffersAction::class),
            ),
        };
    }
}

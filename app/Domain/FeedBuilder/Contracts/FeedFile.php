<?php

namespace App\Domain\FeedBuilder\Contracts;

use App\Domain\FeedBuilder\Data\EntityData;
use App\Domain\FeedBuilder\Data\FeedInfo;

interface FeedFile
{
    public const FILE_DIR = 'feeds/';

    public function create(): void;

    public function fill(EntityData $data): void;

    public function save(string $fileName, string $path = self::FILE_DIR): void;

    public function info(): FeedInfo;
}

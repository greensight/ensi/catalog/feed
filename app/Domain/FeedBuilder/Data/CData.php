<?php

namespace App\Domain\FeedBuilder\Data;

class CData
{
    public function __construct(private string $value)
    {
    }

    public function __toString(): string
    {
        return $this->value;
    }
}

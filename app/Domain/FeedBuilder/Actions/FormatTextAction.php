<?php

namespace App\Domain\FeedBuilder\Actions;

class FormatTextAction
{
    private const REPLACE_SIGN = [
        '"' => '&quot;',
        '&' => '&amp;',
        '>' => '&gt;',
        '<' => '&lt;',
        "'" => '&apos;',
    ];

    public static function execute(?string $string): ?string
    {
        if ($string === null) {
            return null;
        }

        foreach (self::REPLACE_SIGN as $from => $to) {
            $string = str_replace($from, $to, $string);
        }

        return $string;
    }
}

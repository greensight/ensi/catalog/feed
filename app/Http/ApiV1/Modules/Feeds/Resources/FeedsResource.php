<?php

namespace App\Http\ApiV1\Modules\Feeds\Resources;

use App\Domain\Feeds\Models\Feed;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Feed */
class FeedsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'file' => $this->mapPublicFileToResponse($this->file),
            'planned_delete_at' => $this->planned_delete_at,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'feed_settings' => FeedSettingsResource::make($this->whenLoaded('feedSettings')),
        ];
    }
}

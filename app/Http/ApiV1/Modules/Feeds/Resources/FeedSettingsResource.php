<?php

namespace App\Http\ApiV1\Modules\Feeds\Resources;

use App\Domain\Feeds\Models\FeedSettings;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin FeedSettings */
class FeedSettingsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'active' => $this->active,
            'type' => $this->type,
            'platform' => $this->platform,
            'active_product' => $this->active_product,
            'active_category' => $this->active_category,
            'shop_name' => $this->shop_name,
            'shop_url' => $this->shop_url,
            'shop_company' => $this->shop_company,
            'update_time' => $this->update_time,
            'delete_time' => $this->delete_time,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'feeds' => FeedsResource::collection($this->whenLoaded('feeds')),
        ];
    }
}

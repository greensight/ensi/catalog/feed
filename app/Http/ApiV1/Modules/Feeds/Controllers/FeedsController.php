<?php

namespace App\Http\ApiV1\Modules\Feeds\Controllers;

use App\Http\ApiV1\Modules\Feeds\Queries\FeedsQuery;
use App\Http\ApiV1\Modules\Feeds\Resources\FeedsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class FeedsController
{
    public function get(int $id, FeedsQuery $query): Responsable
    {
        return FeedsResource::make($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, FeedsQuery $query): Responsable
    {
        return FeedsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}

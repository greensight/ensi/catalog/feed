<?php

use App\Domain\Cloud\Models\CloudIntegration;
use App\Http\ApiV1\Modules\Cloud\Tests\Factories\CreateCloudIntegrationRequestFactory;
use App\Http\ApiV1\Modules\Cloud\Tests\Factories\PatchCloudIntegrationRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/cloud/cloud-integrations 200', function () {
    /** @var CloudIntegration $model */
    $model = CloudIntegration::factory()->create();

    getJson('/api/v1/cloud/cloud-integrations')
        ->assertJsonPath('data.public_api_key', $model->public_api_key)
        ->assertStatus(200);
});

test('GET /api/v1/cloud/cloud-integrations 404', function () {
    CloudIntegration::factory()->create(['stage' => 'undefined-stage']);

    getJson('/api/v1/cloud/cloud-integrations')
        ->assertStatus(404);
});

test('POST /api/v1/cloud/cloud-integrations 200', function () {
    $request = CreateCloudIntegrationRequestFactory::new()->make();

    postJson('/api/v1/cloud/cloud-integrations', $request)
        ->assertStatus(201);

    assertDatabaseHas(CloudIntegration::class, [
        'public_api_key' => $request['public_api_key'],
        'private_api_key' => $request['private_api_key'],
        'integration' => $request['integration'],
    ]);
});

test('PATCH /api/v1/cloud/cloud-integrations 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    CloudIntegration::factory()->create();

    $request = PatchCloudIntegrationRequestFactory::new()->make();

    patchJson("/api/v1/cloud/cloud-integrations", $request)
        ->assertJsonPath('data.public_api_key', $request['public_api_key'])
        ->assertStatus(200);

    assertDatabaseHas(CloudIntegration::class, [
        'public_api_key' => $request['public_api_key'],
        'private_api_key' => $request['private_api_key'],
        'integration' => $request['integration'],
    ]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/cloud/cloud-integrations 400', function (
    array $prevFields,
    array $request,
) {
    CloudIntegration::factory()->create($prevFields);

    patchJson("/api/v1/cloud/cloud-integrations", $request)
        ->assertStatus(400);
})->with([
    [
        ['integration' => true, 'public_api_key' => '123', 'private_api_key' => '456'],
        ['public_api_key' => null],
    ],
    [
        ['integration' => true, 'public_api_key' => '123', 'private_api_key' => '456'],
        ['private_api_key' => null],
    ],
    [
        ['integration' => true, 'public_api_key' => '123', 'private_api_key' => '456'],
        ['public_api_key' => null, 'private_api_key' => null],
    ],
    [
        ['integration' => false, 'public_api_key' => '', 'private_api_key' => '456'],
        ['integration' => true],
    ],
    [
        ['integration' => false, 'public_api_key' => '123', 'private_api_key' => ''],
        ['integration' => true],
    ],
]);

test('PATCH /api/v1/cloud/cloud-integrations 404', function () {
    $request = PatchCloudIntegrationRequestFactory::new()->make();

    patchJson("/api/v1/cloud/cloud-integrations", $request)
        ->assertStatus(404);
});

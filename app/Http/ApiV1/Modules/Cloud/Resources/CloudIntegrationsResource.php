<?php

namespace App\Http\ApiV1\Modules\Cloud\Resources;

use App\Domain\Cloud\Models\CloudIntegration;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin CloudIntegration
 */
class CloudIntegrationsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'private_api_key' => $this->private_api_key,
            'public_api_key' => $this->public_api_key,
            'integration' => $this->integration,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}

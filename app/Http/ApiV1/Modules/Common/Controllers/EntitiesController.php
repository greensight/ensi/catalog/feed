<?php

namespace App\Http\ApiV1\Modules\Common\Controllers;

use App\Domain\Common\Jobs\MigrateEntitiesJob;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class EntitiesController
{
    public function migrate(): Responsable
    {
        MigrateEntitiesJob::dispatch();

        return new EmptyResource();
    }
}

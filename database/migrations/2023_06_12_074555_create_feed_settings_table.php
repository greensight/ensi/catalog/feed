<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('feed_settings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code')->unique();
            $table->boolean('active')->default(false);
            $table->tinyInteger('type');
            $table->tinyInteger('platform');

            $table->boolean('active_product')->default(false);
            $table->boolean('active_category')->default(false);

            $table->string('shop_name');
            $table->string('shop_url');
            $table->string('shop_company');

            $table->integer('update_time');
            $table->integer('delete_time');
            $table->timestamps(6);
        });

        Schema::dropIfExists('feeds');

        Schema::create('feeds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('feed_settings_id');
            $table->string('code')->unique();
            $table->string('file');

            $table->timestamp('planned_delete_at', 6);
            $table->timestamps(6);

            $table->foreign('feed_settings_id')
                ->on('feed_settings')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('feeds');

        Schema::create('feeds', function (Blueprint $table) {
            $table->id()->index();
            $table->string('name')->nullable(false);
            $table->string('type')->nullable(false);
            $table->integer('created_by')->nullable(false);
            $table->integer('updated_by')->nullable(true)->default(null);
            $table->timestamps();
        });

        Schema::dropIfExists('feed_settings');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }

    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable();
        });

        DB::table('category_product_links')->select(['id', 'product_id', 'category_id'])->chunkById(100, function (Collection $productLinks) {
            foreach ($productLinks as $productLink) {
                DB::table('products')
                    ->where('product_id', $productLink->product_id)
                    ->whereNull('category_id')
                    ->update(['category_id' => $productLink->category_id]);
            }
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable(false)->change();
        });
    }
};

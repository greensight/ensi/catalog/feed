<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('cloud_created')->default(false);
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->boolean('cloud_created')->default(false);
        });
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('cloud_created');
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('cloud_created');
        });
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->timestamp('cloud_fields_updated_at', 6)->useCurrent();
        });
    }

    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('cloud_fields_updated_at');
        });
    }
};
